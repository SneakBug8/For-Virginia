using UnityEngine;
using MarkLight.Views.UI;
using System.Xml;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainView : UIView
{
    public static MainView Global;

    void Awake() {
        Global = this;
    }
    
    public Location curloc;

    public bool Button0_Active = false;
    public bool Button1_Active = false;
    public bool Button2_Active = false;
    
    public void Reload() {
        Debug.Log("Loading Main View.");

        SetValue(() => Button0_Active, false);
        SetValue(() => Button1_Active, false);
        SetValue(() => Button2_Active, false);

        if (curloc.actions.Count>0) {
            SetValue(() => Button0_Active, true);
        }
        if (curloc.actions.Count>1) {
            SetValue(() => Button1_Active, true);
        }
        if (curloc.actions.Count==3) {
            SetValue(() => Button2_Active, true);
        }
        if (curloc.actions.Count>3) {
            Debug.LogError("No button for action in location: "+curloc.name);
        }
    }
	XmlNode XmlRoot;

	// Use this for initialization
	void Start () {
		string locname;

		#if UNITY_EDITOR
		locname = "start";
		# else
		locname = SavesSystem.Global.Get("loc") ?? "start";
		# endif

		Debug.Log("Starting loc: " + locname);

		curloc = FindLocation(locname);

		Reload();

		DrawLoc();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ApplyLocation(string locname) {
		curloc = FindLocation(locname);
		DrawLoc();
	}

	public Location FindLocation(string locname) {
		if (XmlRoot == null) {
			XmlDocument xDoc = new XmlDocument();

			TextAsset textAsset = (TextAsset) Resources.Load("locations"); 
			xDoc.LoadXml (textAsset.text);

			XmlRoot = xDoc.FirstChild;
		}
		
		XmlElement locelement = XmlRoot[locname];

		if (locelement==null) {
			Debug.LogError("No location with name "+locname+" exists.");
			Application.Quit();
			return new Location();
		}

		Location loc = new Location() {
			text = locelement["text"].InnerText,
			name = locname
		};

		if (locelement["function"]!=null) {
			Functions.Global.SendMessage(locelement["function"].InnerText);
		}

		foreach (XmlElement action in locelement["actions"].ChildNodes) {

			Action act = new Action {
				text = action["text"].InnerText,
			};

				if (action["condition"]!=null) {
					if (action["condition"]["item"]!=null) {
						if (!GlobalController.Global.items.list.Contains(action["condition"]["item"].InnerText)) {
							continue;
						}
					}
					if (action["condition"]["variable"]!=null) {
						if (SavesSystem.Global.Get(action["condition"]["variable"].GetAttribute("name"))!=action["condition"]["variable"].InnerText) {
							continue;
						}
					}
				}

				if (action["location"]!=null)
					act.location = action["location"].InnerText;
				if (action["function"]!=null)
					act.function = action["function"].InnerText;
				if (action["scene"]!=null)
					act.scene = action["scene"].InnerText;

			loc.actions.Add(act);
		}
        
		return loc;
	}

	public void DrawLoc(Location loc) {
		// AdsController.Global.LocEvent();
		// AdsController.Global.ShowAd();

		// Deleting btns from prev loc
		/*
		foreach (Button btn in ButtonsParent.GetComponentsInChildren<Button>()) {
			Destroy(btn.gameObject);
		}

		LocatonText.text = loc.text;

		// Rendering new buttons
		int buttonid = 0;

		foreach (Action act in loc.actions) {
			act.button = Instantiate(ButtonPref,ButtonsParent.transform.position,Quaternion.identity).GetComponent<Button>();

			act.button.transform.SetParent(ButtonsParent);
			act.button.transform.localScale = new Vector3(1,1,1);

			act.button.gameObject.GetComponentInChildren<Text>().text=act.text;

			buttonid++;
			
		}
		*/
	}

	public void DrawLoc() {
		DrawLoc(curloc);
	}

	public void OnButtonClick(int id) {
		curloc.actions[id].Execute();
	}
}

public class Location {
	public string name;
	public string text;
	public List<Action> actions = new List<Action>();
}

public class Action {
	public string text;
	public string location;
	public string function;
	public string scene;

	public void Execute() {
		if (function!=null) {
			Functions.Global.gameObject.SendMessage(function);
		}

		if (location!=null) {
			SavesSystem.Global.Set("loc",location);

			MainView.Global.curloc = MainView.Global.FindLocation(location);
			MainView.Global.DrawLoc();

			return;
		}

		if (scene!=null) {
			SceneManager.LoadScene(scene);
		}

		Debug.LogError("No actions assigned to button");
	}
}