﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavesSystem : MonoBehaviour {

	public static SavesSystem Global;

	void Awake() {
		Global = this;
		DontDestroyOnLoad(gameObject);
	}

	public Dictionary<string,string> SavedVariables;

	public void Set(string key, string value)
	{
		PlayerPrefs.SetString(key,value);
		Save();
	}

	public string Get(string key)
	{
		if (!PlayerPrefs.HasKey(key))
			return null;
		return PlayerPrefs.GetString(key, null);
	}

	public void Save() {
		PlayerPrefs.Save();
	}

	public void SaveItems(List<string> items) {
		Set("items",System.String.Join("|",items.ToArray()));
	}

	public List<string> GetItems() {
		if (!PlayerPrefs.HasKey("items"))
			return new List<string>();
		return new List<string>(Get("items").Split('|'));
	}
}
